#!/usr/bin/python
import sys
import datetime

def read_file(filename):
    f = open (filename)

    for line in f:
        if line.startswith("DEL"):
            continue
        yield line.split()

data_by_week = {}
for id, date, time, longitude, latitude in read_file(sys.argv[1]):
    year, month, day = date.split('-')
    week_number = datetime.date(int(year), int(month), int(day)).isocalendar()[1]
    week = "%s-%d" % (year, week_number)
    try:
        data_by_week[week].append((id, date, time, longitude, latitude))
    except:
        data_by_week[week] = [(id, date, time, longitude, latitude)]

for week in data_by_week:
    f = open("%s/%s" % (sys.argv[2], week), "w")
    for line in data_by_week[week]:
        f.write("%s\t%s\t%s\t%s\t%s\n" % line)
    f.close()
